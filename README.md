Sources de construction du handymenu.deb
========================================

dépendance :

    sudo apt update && sudo apt install equivs-build

manuel :

    man equivs-build

commande à lancer pour construire le paquet depuis le dossier source :

    equivs-build handymenu.equivs

qui construira le deb dans le dossier. pour l'installer :

    dpkg -i handymenu_xx.deb

avec *xx* correspondant à la version du handymenu. pour le tester :

    handymenu

pensez à supprimer le dossier de configuration entre les tests :

    rm -Rf ~/.handymenu

----------------------------------------

thème d'icônes utilisé :

* les icônes sont réalisées à partir du thème Clarity par Jakub Jankiewicz http://jcubic.pl GPLv3
* les sources intèrent le fichier 'xcf' pour the gimp afin de créer/éditer vos icônes
* le fichier xcf n'est pas intégré dans le paquet handymenu par défaut et contient des icônes en plus

----------------------------------------

